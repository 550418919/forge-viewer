const { AuthClientTwoLegged } = require('forge-apis');

const config = require('../../config');

function getClient(scopes) {
    const { client_id, client_secret } = config.credentials;
    return new AuthClientTwoLegged(client_id, client_secret, scopes || config.scopes.internal);
}

let cache = new Map();
async function getToken(scopes) {
    const key = scopes.join('+');
    //判断token缓存
    if (cache.has(key) && cache.get(key).expires_at > Date.now()) {
        return cache.get(key);
    }
    //获取forge客户端
    const client = getClient(scopes);
    let credentials = await client.authenticate();
    //刷新token时间
    credentials.expires_at = Date.now() + credentials.expires_in * 1000;
    //重置缓存
    cache.set(key, credentials);
    return credentials;
}

//获取公开token
async function getPublicToken() {
    return getToken(config.scopes.public);
}

async function getInternalToken() {
    return getToken(config.scopes.internal);
}

module.exports = {
    getClient,
    getPublicToken,
    getInternalToken
};
