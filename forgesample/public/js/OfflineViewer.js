$(document).ready(function () {
    var viewer, viewer2d;
    var config = {
        extensions: [
            "Autodesk.Viewing.ZoomWindow",
            "Autodesk.AEC.Minimap3DExtension",
            "Autodesk.AEC.LevelsExtension",
            'Autodesk.DocumentBrowser'
        ],
        disabledExtensions: {
            measure: false,
            section: false,
        },
        memory: {
            limit: 32 * 1024    //32 GB
        }
    };

    var options = {
        env: 'Local',
        offline: 'true',
        useADP: false,
        offlineResourcePrefix: location.href.substring(0, location.href.lastIndexOf('/'))
    };
    var documentId = decodeURIComponent(options.offlineResourcePrefix + "/3d.svf");
    Autodesk.Viewing.Initializer(options, function () {
        show3dviewer();
        show2dviewer();
    });

    function show3dviewer() {
        var myViewerDiv = document.getElementById('forgeViewer3D');
        viewer = new Autodesk.Viewing.Private.GuiViewer3D(myViewerDiv, config);
        viewer.start(documentId, { isAEC: true }, onSuccess, onFailure);
        viewer.addEventListener(Autodesk.Viewing.AGGREGATE_SELECTION_CHANGED_EVENT, function (event) {
            if (event.selections && (db3d == null || db3d != event.selections[0].dbIdArray[0])) {
                console.log("click3d", event.selections[0].dbIdArray);
                db3d = event.selections[0].dbIdArray[0];
                viewer2d.select(event.selections[0].dbIdArray)
            }
        })
    }

    function show2dviewer() {
        var myViewerDiv = document.getElementById('forgeViewer2D');
        viewer2d = new Autodesk.Viewing.Private.GuiViewer3D(myViewerDiv);
        viewer2d.start(documentId, { isAEC: true }, onSuccess, onFailure);
        viewer2d.addEventListener(Autodesk.Viewing.AGGREGATE_SELECTION_CHANGED_EVENT, function (event) {
            if (event.selections && (db2d == null || db2d != event.selections[0].dbIdArray[0])) {
                console.log("click2d", event.selections[0].dbIdArray);
                db2d = event.selections[0].dbIdArray[0];
                viewer.select(event.selections[0].dbIdArray);
            }
        })
    }

    var db2d, db3d;

    //获取2d图纸，取其中一个node显示
    function getManifestList(doc) {
        const filterGeom = (geom) => ['2d'].indexOf(geom.data.role) !== -1;
        const geometries = doc.getRoot().search({ type: 'geometry' }).filter(filterGeom);
        console.log("geometries", geometries);
        var guid = "6d3acd40-53b7-41b4-9d96-72e9eaf4bc89-0005d699";
        //获取节点
        var viewables = (guid ? doc.getRoot().findByGuid(guid) : doc.getRoot().getDefaultGeometry());
        viewer2d.loadDocumentNode(doc, viewables).then(i => {

        });

    }

    function onSuccess(document) {
        var documentPath = decodeURIComponent(options.offlineResourcePrefix + "/manifest-model.json");
        //加载manifest
        Autodesk.Viewing.Document.load(documentPath, (doc) => {
            if (viewer2d) {
                getManifestList(doc);
            }
            if (viewer) {
                doc.downloadAecModelData();
                var viewable = doc.getRoot().getDefaultGeometry();
                var options = {};
                //小地图
                viewer.loadDocumentNode(doc, viewable, options).then(i => {
                    viewer.loadExtension('Autodesk.AEC.Minimap3DExtension');
                });

            }
        });
    }

    function onFailure(viewerErrorCode, viewerErrorMsg) {
        console.error('onDocumentLoadFailure() - errorCode:' + viewerErrorCode + '\n- errorMessage:' + viewerErrorMsg);
    }


});