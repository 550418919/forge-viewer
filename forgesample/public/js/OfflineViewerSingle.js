$(document).ready(function () {
    var viewer;
    var config = {
        extensions: [
            "Autodesk.Viewing.ZoomWindow",
            "Autodesk.AEC.Minimap3DExtension",
            "Autodesk.AEC.LevelsExtension",
            'Autodesk.DocumentBrowser'
        ],
        disabledExtensions: {
            measure: false,
            section: false,
        },
        memory: {
            limit: 32 * 1024    //32 GB
        }
    };

    var options = {
        env: 'Local',
        offline: 'true',
        useADP: false,
        offlineResourcePrefix: location.href.substring(0, location.href.lastIndexOf('/'))
    };
    var documentId = decodeURIComponent(options.offlineResourcePrefix + "/3d.svf");
    Autodesk.Viewing.Initializer(options, function () {
        var myViewerDiv = document.getElementById('forgeViewer');
        viewer = new Autodesk.Viewing.Private.GuiViewer3D(myViewerDiv, config);
        viewer.start(documentId, { isAEC: true }, onSuccess, onFailure);
    });

    function onSuccess(document) {

        //注册NestedViewerExtension
        Autodesk.Viewing.theExtensionManager.registerExternalExtension(
            'NestedViewerExtension',
            '/extensions/NestedViewerExtension/contents/main.js');

        var documentPath = decodeURIComponent(options.offlineResourcePrefix + "/manifest-model.json");
        //加载manifest
        Autodesk.Viewing.Document.load(documentPath, (doc) => {
            doc.downloadAecModelData();
            var viewable = doc.getRoot().getDefaultGeometry();
            var options = {};
            //小地图
            viewer.loadDocumentNode(doc, viewable, options).then(i => {
                viewer.loadExtension('Autodesk.AEC.Minimap3DExtension');
            });

            //documentLoadSuccess
            viewer.addEventListener(Autodesk.Viewing.TEXTURES_LOADED_EVENT, () => {
                // viewer.getExtension("Autodesk.BimWalk").activate();

                //加载NestedViewerExtension
                viewer.loadExtension("NestedViewerExtension", { filter: ["2d", "3d"], crossSelection: true })
            });

        });
    }

    function onFailure(viewerErrorCode, viewerErrorMsg) {
        console.error('onDocumentLoadFailure() - errorCode:' + viewerErrorCode + '\n- errorMessage:' + viewerErrorMsg);
    }



    function aggergated() {

        // -----------设定目前选择集内容--------
        //多模型获取选择集
        // var selections = viewer.getAggregateSelection();

        // //多模型选择集
        // viewer.impl.selector.setAggregateSelection([
        //     {
        //         ids: [2102, 1025],
        //         model: modelAR//要载入的model
        //     },
        // ]);
        //------------------监听选择集变更事件监听-------------------

        // 单模型：Autodesk.Viewing.SELECTION_CHANGED_EVENT
        // 多模型：Autodesk.Viewing.AGGREGATE_SELECTION_CHANGED_EVENT

        //------------------取得目前被隔离显示的元件-----

        // 单模型：viewer.getIsolateNodes();
        // 多模型：viewer.impl.visibilityManager.getAggregateIoslateNodes();


        //---------设定要隔离显示的元件----
        // 单模型：viewer.isolate(4023);
        // 多模型：viewer.impl.visibilityManager.aggregateIsolate([{ids:[],model:modelAR}]);


        // //---------隔离显示变更事件监听//---------

        // 隔离显示变更监听 单模型： Autodesk.Viewing.ISOLATE_EVENT
        // 隔离显示变更监听 多模型： Autodesk.Viewing.AGGREGATE_ISOLATE_CHANGED_EVENT


        // ---------取得目前被隐藏的元件---------
        // 单模型：viewer.getHiddenNodes();
        // 多模型：viewer.impl.visibilityManager.getAggregateHiddenNodes();


        // ---------设置目前被隐藏的元件---------
        // view.hide(4032);
        // 多模型：viewer.impl.visibilityManager.aggregateHidden([{ids:[4032],model:modelAR}]);

        // ---------元件隐藏状态变更事件监听---------
        // 单模型： Autodesk.Viewing.HIDE_EVENT
        // 多模型： Autodesk.Viewing.AGGREGATE_HIDDEN_CHANGED_EVENT


        // 单模型改变模型外观api
        // viewer.setThemingColor(3010,color);
        // viewer.hide([2012]);
        // viewer.show([2012]);
        // viewer.fitToview([2012]);

        // 多模型改变模型外观api
        // viewer.setThemingColor(3010,color,model);
        // viewer.hide([2012],model);
        // viewer.show([2012],model);
        // viewer.fitToview([2012],model);


        // 多模型取得已载入的模型
        // viewer.impl.modelQueue.getModels();

        // 取得元件属性
        // 单模型：viewer.getProperties(3030,(result)=>console.log(result));
        // 多模型：通过 const models = viewer.impl.modelQueue.getModels().concat();拿到model
        // const model = models[0]; 通过model拿到属性   
        // model.getProperties(3030,(result)=>console.log(result));

        // aggergatedView 

    }

});