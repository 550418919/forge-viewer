
//版本
const CACHE_VERSION = 1.1;
//名称
const CACHE_NAME = 'cache-sapmle' + CACHE_VERSION;
//指定缓存文件
const urlsToCache = [
    '/css/main.css',
    '/jquery.min.js',
    '/Autodesk-Forge-Viewer_v7.52/viewer3D.min.js',
    '/Autodesk-Forge-Viewer_v7.52/extensions/NestedViewerExtension/contents/main.js',
    '/Autodesk-Forge-Viewer_v7.52/extensions/NestedViewerExtension/contents/main.css',
    '/js/OfflineViewer.js'
];
//缓存白名单
const cacheWhiteList = [
];

//                                        |->  Error             <->Terminated
// 生命周期: no serviceWorker -> installing -> Activated - > Idle <-> Fetch/Message


//注册成功触发 install事件
this.addEventListener("install", event => {
    //event.waitUntil： serviceWorker运行起来
    //oninstall和onactivate完成前需要一些时间
    event.waitUntil(
        //开启缓存
        caches.open(CACHE_NAME).then(cache => {
            //传入文件数组告诉需要缓存的文件
            return cache.addAll(urlsToCache);
        })
    );
});

//删除旧版本的缓存
this.addEventListener("activate", event => {
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames.map(cacheName => {
                    // 和当前版本号不一致
                    if (CACHE_NAME != cacheName) {
                        return caches.delete(cacheName);
                    }
                    // 不在白名单
                    // if (cacheWhiteList.indexOf(cacheName) === -1) {
                    //     return caches.delete(cacheName);
                    // }
                })
            )
        })
    );
});

//用户导航到另一个页面或者刷新页面
this.addEventListener("fetch", event => {
    const url = new URL(event.request.url);
    if (url.origin === location.origin && urlsToCache.indexOf(url.pathname) > -1) {
        //从cacheName下的所有的service worker缓存的文件中查找结果，如果有一个匹配上response的，就返回缓存的值，
        //否则就返回调用fetch的结果，也就是发起网络请求，并返回收到的数据
        event.respondWith(
            caches.match(event.request).then(response => {
                if (response) {
                    console.log("fetch ", event.request.url, "有缓存，从缓存中取");
                    return response;
                }
                console.log("fetch ", event.request.url, "没有缓存，网络获取");
                return fetch(event.request).then(
                    response => {
                        //取完后加入缓存
                        return caches.open(CACHE_NAME).then(cache => {
                            cache.put(event.request, response.clone())
                            return response;
                        });
                    }
                );
            })
        );
    }
});