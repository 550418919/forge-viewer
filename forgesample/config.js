// Autodesk Forge configuration
module.exports = {
    // 设置key
    credentials: {
        client_id: "xNMeq5TrwHJkGZQXAMXWGmE4KlwSRwYm",
        client_secret: "4Eqfj5rqec7wEPAt",
        callback_url: "http://localhost:3000/api/forge/callback/oauth"
    },
    scopes: {
        // 内部token权限
        // 需要写入访问权限，因此内部 token 将使用 bucket:create、bucket:read、data:read、data:create 和 data:write 范围
        internal: ['bucket:create', 'bucket:read', 'data:read', 'data:create', 'data:write'],
        //公共token权限
        public: ['viewables:read']
    }
};
